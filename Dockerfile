# Base image
FROM apache/rocketmq:4.9.4

# Set environment variables
ENV NAMESRV_ADDR=rmqnamesrv:9876 \
    JAVA_OPT_EXT="-server -Xms64m -Xmx64m -Xmn64m"

# Copy broker configuration file
COPY ./config/broker.conf /opt/rocketmq-4.9.4/conf/broker.conf

# Expose ports
EXPOSE 10909 10911

# Start broker
CMD ["sh", "mqbroker", "-c", "/opt/rocketmq-4.9.4/conf/broker.conf"]


# Base image
FROM apache/rocketmq:4.9.4

# Expose ports
EXPOSE 9876

# Start nameserver
CMD ["sh", "mqnamesrv"]


# Base image
FROM apacherocketmq/rocketmq-dashboard:1.0.0

# Set environment variables
ENV JAVA_OPTS="-Drocketmq.config.namesrvAddr=rmqnamesrv:9876 -Dserver.port=8180 -Drocketmq.config.isVIPChannel=false"

# Expose port
EXPOSE 8180

# Start dashboard
CMD ["java", "-jar", "/opt/rocketmq-dashboard.jar"]